package edu.kashis.lab2;

import java.util.ArrayList;

import edu.kashis.lab1.Card;
import edu.kashis.lab1.Rank;

public class Controller {
	MessagePanel msg;
	boolean playing;
	State state;
	
	boolean p1lost = false, p2lost = false, p3lost = false;
	
	public Controller () {}
	
	public void init() {
		state = new State();
		msg.outputMessage(1, "It is your hand. Your hand: "
				+ state.getPlayer(1).getHandString()
				+ " Dealer's hand: "
				+ state.getDealer().getHandString());
		msg.outputMessage(2, Constants.WAIT_MSG);
		msg.outputMessage(3, Constants.WAIT_MSG);
	}
	
	public void clientRequest(char c) {
		if (p1lost&&p2lost&&p3lost) {
			playing = false;
			msg.showMenu();
		}
		if (playing) {
			//Decide based on c
			changeState(c);
			return;
		}
		p1lost = false; p2lost = false; p3lost = false;
		init();
		playing = true;
	}
	
	public void setMessenger(MessagePanel message) {
		msg = message;
		msg.showMenu();
	}
	
	private void showErrorMsgPlayerKey(char c) {
		int player = -1;
		if (c == 'q' || c == 'w') {
			if (p1lost) return;
			player = 1;
		}
		else if (c == 'a' || c == 's') {
			if (p2lost) return;
			player = 2;
		}
		else if (c == 'z' || c == 'x') {
			if (p3lost) return;
			player = 3;
		}
		msg.outputMessage(player, Constants.WRONG_TURN_MSG);
	}
	
	private void changeState(char c) {
		//Assignment #2
		if (c != 'q' && c != 'w' && c != 'a' && c != 's' && c != 'z' && c != 'x') {
			return; //Invalid key entry
		}
		//wrong turn ..
		if (state.getTurn()%3==1) { //player 1's turn
			if (c == 'a' || c == 's' || c == 'z' || c == 'x') {
				showErrorMsgPlayerKey(c);
				return;
			}
		}
		else if (state.getTurn()%3==2) { //player 2's turn
			if (c == 'q' || c == 'w' || c == 'z' || c == 'x') {
				showErrorMsgPlayerKey(c);
				return;
			}
		}
		else if (c == 'q' || c == 'w' || c == 'a' || c == 's') {
			showErrorMsgPlayerKey(c);
			return;
		}
		//right turn
		if (state.getTurn()%3 == 1 && !p1lost) { //player 1 turn
			if (c == 'q') {
				state.getPlayer(1).receiveCard(state.getDeck().dealCard());
				ArrayList<Card> pHand = state.getPlayer(1).getHand();
				int pSum = 0, aceCount = 0;
				for (Card card : pHand) {
					if (card.getRank() == Rank.ACE) { aceCount++; continue; }
					if (card.getRank().ordinal()>10) { pSum += 10; continue; }
					pSum += card.getRank().ordinal()+1;
				}
				while (aceCount>0) {
					if (pSum+11>21) pSum++;
					else pSum+=11;
					aceCount--;
				}
				if (pSum>21) {
					msg.outputMessage(1, Constants.LOST);
					p1lost = true;
					state.nextTurn();
					msg.outputMessage(2, "It is your hand. Your hand: "
							+ state.getPlayer(2).getHandString()
							+ " Dealer's hand: "
							+ state.getDealer().getHandString());
					return;
				}
				else if (pSum==21) {
					msg.outputMessage(1, Constants.WON);
					playing = false;
					return;
				}
				msg.outputMessage(1, "It is your hand. Your hand: "
						+ state.getPlayer(1).getHandString()
						+ " Dealer's hand: "
						+ state.getDealer().getHandString());
				return;
			}
			else if (c == 'w') {
				ArrayList<Card> pHand = state.getPlayer(1).getHand();
				int pSum = 0, aceCount = 0;
				for (Card card : pHand) {
					if (card.getRank() == Rank.ACE) { aceCount++; continue; }
					if (card.getRank().ordinal()>10) { pSum += 10; continue; }
					pSum += card.getRank().ordinal()+1;
				}
				while (aceCount>0) {
					if (pSum+11>21) pSum++;
					else pSum+=11;
					aceCount--;
				}
				if (pSum==21&&pHand.size()==2) {
					msg.outputMessage(1, Constants.BLACKJACK);
					playing = false;
					return;
				}
				state.nextTurn();
				if (p2lost) state.nextTurn();
				if (p3lost) state.nextTurn();
				msg.outputMessage(1, Constants.WAIT_MSG);
				msg.outputMessage(2, "It is your hand. Your hand: "
						+ state.getPlayer(2).getHandString()
						+ " Dealer's hand: "
						+ state.getDealer().getHandString());
				return;
			}
		}
		if (state.getTurn()%3 == 2 && !p2lost) { //player 2 turn
			if (c == 'a') {
				state.getPlayer(2).receiveCard(state.getDeck().dealCard());
				ArrayList<Card> pHand = state.getPlayer(2).getHand();
				int pSum = 0, aceCount = 0;
				for (Card card : pHand) {
					if (card.getRank() == Rank.ACE) { aceCount++; continue; }
					if (card.getRank().ordinal()>10) { pSum += 10; continue; }
					pSum += card.getRank().ordinal()+1;
				}
				while (aceCount>0) {
					if (pSum+11>21) pSum++;
					else pSum+=11;
					aceCount--;
				}
				if (pSum>21) {
					msg.outputMessage(2, Constants.LOST);
					p2lost = true;
					state.nextTurn();
					msg.outputMessage(3, "It is your hand. Your hand: "
							+ state.getPlayer(3).getHandString()
							+ " Dealer's hand: "
							+ state.getDealer().getHandString());
					return;
				}
				else if (pSum==21) {
					msg.outputMessage(2, Constants.WON);
					playing = false;
					return;
				}
				msg.outputMessage(2, "It is your hand. Your hand: "
						+ state.getPlayer(2).getHandString()
						+ " Dealer's hand: "
						+ state.getDealer().getHandString());
				return;
			}
			else if (c == 's') {
					ArrayList<Card> pHand = state.getPlayer(2).getHand();
					int pSum = 0, aceCount = 0;
					for (Card card : pHand) {
						if (card.getRank() == Rank.ACE) { aceCount++; continue; }
						if (card.getRank().ordinal()>10) { pSum += 10; continue; }
						pSum += card.getRank().ordinal()+1;
					}
					while (aceCount>0) {
						if (pSum+11>21) pSum++;
						else pSum+=11;
						aceCount--;
					}
					if (pSum==21&&pHand.size()==2) {
						msg.outputMessage(2, Constants.BLACKJACK);
						playing = false;
						return;
					}
					state.nextTurn();
					if (p3lost) state.nextTurn();
					if (p1lost) state.nextTurn();
					msg.outputMessage(2, Constants.WAIT_MSG);
					msg.outputMessage(3, "It is your hand. Your hand: "
							+ state.getPlayer(3).getHandString()
							+ " Dealer's hand: "
							+ state.getDealer().getHandString());
					return;
			}
		}
		if (state.getTurn()%3 == 0 && !p3lost) { //player 3 turn
			if (c == 'z') {
				state.getPlayer(3).receiveCard(state.getDeck().dealCard());
				ArrayList<Card> pHand = state.getPlayer(3).getHand();
				int pSum = 0, aceCount = 0;
				for (Card card : pHand) {
					if (card.getRank() == Rank.ACE) { aceCount++; continue; }
					if (card.getRank().ordinal()>10) { pSum += 10; continue; }
					pSum += card.getRank().ordinal()+1;
				}
				while (aceCount>0) {
					if (pSum+11>21) pSum++;
					else pSum+=11;
					aceCount--;
				}
				if (pSum>21) {
					msg.outputMessage(3, Constants.LOST);
					p3lost = true;
					return;
				}
				else if (pSum==21) {
					msg.outputMessage(3, Constants.WON);
					playing = false;
					return;
				}
				msg.outputMessage(3, "It is your hand. Your hand: "
						+ state.getPlayer(3).getHandString()
						+ " Dealer's hand: "
						+ state.getDealer().getHandString());
				return;
			}
			else if (c == 'x') {
				//insert dealer actions here
				ArrayList<Card> dHand = state.getDealer().getHand();
				int dSum = 0, aceCount = 0;
				for (Card card : dHand) {
					if (card.getRank() == Rank.ACE) { aceCount++; continue; }
					dSum += card.getRank().ordinal();
				}
				while (aceCount>0) {
					if (dSum+11>21) dSum++;
					else dSum+=11;
					aceCount--;
				}
				ArrayList<Card> pHand = state.getPlayer(3).getHand();
				int pSum = 0, paceCount = 0;
				for (Card card : pHand) {
					if (card.getRank() == Rank.ACE) { paceCount++; continue; }
					if (card.getRank().ordinal()>10) { pSum += 10; continue; }
					pSum += card.getRank().ordinal()+1;
				}
				while (paceCount>0) {
					if (pSum+11>21) pSum++;
					else pSum+=11;
					aceCount--;
				}
				if (pSum==21&&pHand.size()==2) {
					msg.outputMessage(3, Constants.BLACKJACK);
					playing = false;
					return;
				}
				if (pSum==21&&pHand.size()!=2) {
					msg.outputMessage(3,  Constants.WON);
					playing = false;
					return;
				}
				if (pSum>21) {
					msg.outputMessage(3,  Constants.LOST);
					return;
				}
				msg.outputMessage(1, "It is your hand. Your hand: "
						+ state.getPlayer(1).getHandString()
						+ " Dealer's hand: "
						+ state.getDealer().getHandString());
				state.nextTurn();
				if (p1lost) state.nextTurn();
				if (p2lost) state.nextTurn();
				msg.outputMessage(3, Constants.WAIT_MSG);
				return;
			}
		}
	}
}









