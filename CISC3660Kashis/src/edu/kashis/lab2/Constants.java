package edu.kashis.lab2;

public class Constants {
	final static String WAIT_MSG = "Wait for other players to finish their turn";
	final static String WRONG_TURN_MSG = "IT IS NOT YOUR TURN!";
	final static String WON = "You won!";
	final static String LOST = "You lost! :(";
	final static String BLACKJACK = "Blackjack!";
	public final static int MAX_NUM_PLAYERS = 4; //Including the dealer
}
