package edu.kashis.lab2;

import edu.kashis.lab1.Deck;

public class State {
	private byte turn;
	private Player [] players; //player[0] is dealer
	private Deck deck;
	public State() {
		init();
	}
	public void init() {
		turn = 1;
		deck = new Deck();
		deck.initFullDeck();
		deck.shuffle();
		players = new Player[Constants.MAX_NUM_PLAYERS]; //dealer + 3 new players
		players[0] = new Player();
		players[0].receiveCard(deck.dealCard());
		//Give 2 cards to each player
		for (int i = 1; i < Constants.MAX_NUM_PLAYERS; i++)
		{
			players[i] = new Player();
			players[i].receiveCard(deck.dealCard());
			players[i].receiveCard(deck.dealCard());
		}
	}
	public Player getPlayer(int playerNum) {
		return players[playerNum];
	}
	public Player getDealer() {
		return players[0];
	}
	public Deck getDeck() {return deck;}
	public byte getTurn() {return turn;}
	public void nextTurn() {turn++;}
}
