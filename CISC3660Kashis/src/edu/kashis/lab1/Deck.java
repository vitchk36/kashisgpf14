package edu.kashis.lab1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Deck {
	private ArrayList<Card> deck = new ArrayList<Card>();
	
	
	public Deck() {
	}
	
	public void initFullDeck() {
		deck.removeAll(deck);
		for (Rank r : Rank.values()) {
			for (Suit s : Suit.values()) {
				deck.add(new Card(s, r));
			}
		}
	}
	
	public void initEmptyDeck() {
		deck.removeAll(deck);
	}
	
	public List<Card> getDeck() {
		return Collections.unmodifiableList(deck);
	}
	
	public Card removeCardDeck(int index) {
		return deck.remove(index);
	}
	
	public void addOneCard(Card c) {
		deck.add(c);
	}
	
	public void removeAParticularCard(Card c) {
		for (int i=deck.size()-1; i>=0; i--){
			if (deck.get(i).getRank() == c.getRank() && deck.get(i).getSuit() == c.getSuit()) {
				deck.remove(i);
				break;
			}
		}
	}
	
	public void removeAllCardsOfRank(Rank r) {
		for (int i=deck.size()-1; i>=0; i--) {
			if (deck.get(i).getRank() == r) {
				deck.remove(i);
			}
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		for (Card c : deck) {
			s += c.getSuit().toString() + " " + c.getRank().toString() + "\n";
		}
		return s;
	}
	
	public ArrayList<Card> getOrderedCards() {
		ArrayList<Card> cards = new ArrayList<Card>(52);
		for (Card c : deck)
			for (Rank r : Rank.values())
				if (c.getRank() == r)
					cards.add(c);
		return cards;
	}
	
	public int getNumberOfCardsRemaining() {
		int cardsLeft = 0;
		for (Card c : deck)
			cardsLeft++;
		return cardsLeft;
	}
	
	public Card dealCard() {
		Random rand = new Random();
		int randNum = rand.nextInt(getNumberOfCardsRemaining());
		Card result = removeCardDeck(randNum);
		return result;
	}
	
	public void shuffle() {
		int cL = getNumberOfCardsRemaining();
		ArrayList<Card> cards = new ArrayList<Card>(cL);
		for (int i = 0; i < cL; i++)
			cards.add(dealCard());
		this.deck = cards;
	}
}
