package edu.cisc3660.lab1;

public class Main {
	public static void main(String[] args) {
		Deck deck = new Deck();
		System.out.println(deck.toString());
		deck.shuffle();
		int arr1Sum = 0, arr2Sum = 0;
		Card[] cardArr1 = new Card[10];
		for (int i = 0;i < cardArr1.length; i++) {
			cardArr1[i] = deck.removeCardDeck(0);
			arr1Sum += cardArr1[i].getRank().ordinal();
		}
		Card[] cardArr2 = new Card[10];
		for (int i = 0;i < cardArr2.length; i++) {
			cardArr2[i] = deck.removeCardDeck(0);
			arr2Sum += cardArr2[i].getRank().ordinal();
		}
		if (arr1Sum > arr2Sum) 
			System.out.println ("Array 1 is the winner with a sum of " + arr1Sum);
		else if (arr2Sum > arr1Sum)
			System.out.println("Array 2 is the winner with a sum of " + arr2Sum);
		else System.out.println("The arrays are tied with a sum of " + arr1Sum);
	}
}
