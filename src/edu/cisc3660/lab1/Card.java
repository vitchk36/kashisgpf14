package edu.cisc3660.lab1;

public class Card {
	private Suit suit;
	private Rank rank;
	
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	public Suit getSuit() {
		return suit;
	}
	
	public void setSuit(Suit suit) {
		this.suit = suit;
	}
	
	public Rank getRank() {
		return rank;
	}
	
	public void getRank(Rank rank) {
		this.rank = rank;
	}
	
	public static boolean compare(Card c1, Card c2) {
		return c1.rank == c2.rank && c1.suit == c2.suit;
	}
	
	public static boolean compareRank(Card c1, Card c2) {
		return c1.rank == c2.rank;
	}
	
	public static boolean compareSuit(Card c1, Card c2) {
		return c1.suit == c2.suit;
	}

}
