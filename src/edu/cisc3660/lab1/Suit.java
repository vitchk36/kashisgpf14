package edu.cisc3660.lab1;

public enum Suit {
	CLUBS, DIAMONDS, HEARTS, SPADES;
}
